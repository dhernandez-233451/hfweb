<?php
session_start();
if(isset($_SESSION["tipo"])) if ($_SESSION["tipo"]=='1') header("Location:index_traba.php"); else echo'';
else header('Location:../');
$rows=$_SESSION["rows"];
$calif=$_SESSION["calif"];
$limite_i=$_SESSION["limite_i"];
$total_rows=$_SESSION["total_rows"];
$parametros=$_SESSION["parametros"];
$categorias=$_SESSION["categorias"];
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <title>Página de inicio</title>

    <!-- Para el Bucstra -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">

    <!-- FUENTES  -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link href="css/main.css" rel="stylesheet">
    <!-- OPEN SANS -->
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link rel="stylesheet" href="node_modules/jquery-bar-rating/dist/themes/fontawesome-stars.css">


</head>
<nav class="navbar fixed-top navbar-toggleable-md navbar-light bg-faded">
  <button class="navbar-toggler navbar-toggler-right custom-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand page-scroll" href="#">Hermes Finder</a>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto"><li class="nav-item"><a class="nav-link" href="#"></a></li></ul>
    </li>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Página principal</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Cerrar sesión</a>
      </li>
    </ul>
  </div>
</nav>
<body>

    <!-- SECCIÓN DE TARJETAS -->
    <section class="tarjetas">
      <div class="container text-center warpx">
        <nav class="breadcrumb">
  <a class="breadcrumb-item" href="#">Inicio</a>
  <span class="breadcrumb-item active">Búsqueda</span>
</nav>
        <div class="row">
          <div class="col-md-3" style="margin-top:20px;">
            <!-- Búsqueda -->
            <form id="buscar" name="buscar" action="phscripts/busqueda.php" method="post" style="background: #fff; padding:10px; border-radius:5px; border-width:3px; border-color:#eee;">
              <label for="search" class="sr-only">Búsqueda</label>
              <input type="text" class="form-control" name="bus" id="search" placeholder="Búsqueda" <?php if($parametros['bus']!="")print 'value="'.$parametros['bus'].'"'; ?>>
              </br>
				<!--<input type="hidden" id="limite_i" name="limite_i" value="<?php print $limite_i; ?>">-->
              <button style="widht:100%;" type="submit" class="btn btn-primary">Buscar</button>
            

            <div class="list-group" style="background: #fff; margin-top:10px; padding:10px; border-radius:5px;">
      <h5 class="text-left">Categorías:</h5>
      <button onclick="location.href='phscripts/busqueda.php?cat=Salud'" type="button" class="list-group-item list-group-item-action">Salud</button>
      <button onclick="location.href='phscripts/busqueda.php?cat=Mano%20de%20obra'" type="button" class="list-group-item list-group-item-action">Mano de obra</button>
      <button onclick="location.href='phscripts/busqueda.php?cat=Hogar%20y%20jardinería'" type="button" class="list-group-item list-group-item-action">Hogar y jardinería</button>
      <div class="dropdown">
      <!--<button class="btn btn-secondary dropdown-toggle text-left" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ver más
      </button>-->
      <div class="form-group">
                <select name="cat" style="margin-top:10px;" class="form-control" id="exampleSelect1" onChange="this.form.submit()"><!--O quitar el onChange para un filtro más presiso-->
                  <option>Todas las categorías...</option>
                  <?php for($i=0;$i<count($categorias);$i++){
					  if($parametros['cat']==$categorias[$i]['area'])print '<option selected="selected">'.$categorias[$i]['area'].'</option>';
					  else print '<option>'.$categorias[$i]['area'].'</option>';
				  }?>
                  <option>Otros</option>
                </select>
              </div>
      <!--<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a  class="dropdown-item" href="phscripts/busqueda.php">Todas las categorías...</a>
        <a  class="dropdown-item" href="phscripts/busqueda.php?cat=Autos%20y%20transporte">Autos y transporte</a>
        <a  class="dropdown-item" href="phscripts/busqueda.php?cat=Leyes%20y%20finanzas">Leyes y finanzas</a>
        <a  class="dropdown-item" href="phscripts/busqueda.php?cat=Computación%20e%20internet">Computación e internet</a>
        <a  class="dropdown-item" href="phscripts/busqueda.php?cat=Belleza">Belleza</a>
        <a  class="dropdown-item" href="phscripts/busqueda.php?cat=Negocios%20locales">Negocios locales</a>
        <a  class="dropdown-item" href="phscripts/busqueda.php?cat=Eduación">Educación</a>
        <a  class="dropdown-item" href="phscripts/busqueda.php?cat=Modas%20y%20sastrería">Modas y sastrería</a>
        <a  class="dropdown-item" href="phscripts/busqueda.php?cat=Seguridad%20y%20vigilancia">Seguridad y vigilancia</a>
        <a  class="dropdown-item" href="phscripts/busqueda.php?cat=Eventos">Eventos</a>
        <a  class="dropdown-item" href="phscripts/busqueda.php?cat=Arte%20y%20cultura">Arte y cultura</a>
        <a  class="dropdown-item" href="phscripts/busqueda.php?cat=Deporte%20y%20fitness">Deporte y fitness</a>
        <a  class="dropdown-item" href="phscripts/busqueda.php?cat=otros">Otros</a>
      </div>-->
    </div>
    </div>
    </form>
          </div>



          <div class="col-md-9" style="margin-top:10px;">
          <div class="row">

                <!-- EMPIEZAN LAS TARJETAS -->
<?php for($i=0;$i<count($rows);$i++){?>
            <div class="col-sm">
              <div class="card" style="width: 15rem;">
                <div class="tarj-cab" style="background:url('img/<?=$rows[$i]["imagen"]?>')no-repeat center;"></div>
                <div class="card-block text-left">
                  <h4 class="card-title"><?=$rows[$i]["nombreS"]?></h4>
                  <h6 style="color:#999;"><?=$rows[$i]["nombre"]?></h6>
                  <select class="ex" id="example<?=$i?>" name="example<?=$i?>">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                  <p class="text-right">$<?=$rows[$i]["precio"]?>.00 / <?=$rows[$i]["cualitativo"]?></p>
                  <a href="verdetall.php?rown=<?php echo($i)?>" class="btn btn-primary">Ver más...</a>
                </div>
              </div>
            </div>

<?php } if(count($rows)==0){echo("<h3 style='text-center;padding:30px;'>No se han encontrado resultados para la búsqueda que realizó...</h3>");}
?>

            <!-- FIN DE COLUMNAS -->
          </div>

  <nav aria-label="Page navigation example" style="margin-top:20px;">
  <ul class="pagination justify-content-center">
    <li class="page-item">
    <?php if($limite_i!=0) print '<a class="page-link" href="phscripts/busqueda.php?limite_i='.($limite_i-6).'" onclick="document.forms[\'buscar\'].submit(); return true;">Anterior</a>';  ?>
    </li>
    <!--<li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>-->
    <li class="page-item">
    <?php if($total_rows!=0&&($limite_i+6)<$total_rows)print '<a class="page-link" href="phscripts/busqueda.php?limite_i='.($limite_i+6).'" onclick="document.forms[\'buscar\'].submit(); return true;">Siguiente</a>';?>
    </li>
  </ul>
</nav>
          </div>
        </div>
      </div>
    </section>



    <footer>
        <div class="container">
            <div class="row">
              <div class="col-md-4">
                <ul class="list-inline quicklinks">
                    <li class="list-inline-item">  <span class="copyright">Copyright &copy; Hermes Finder</span>
                    </li>
                </ul>
                </div>
                <div class="col-md-4 text-center">
                    <ul class="list-inline social-buttons">
                        <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 text-center">
                    <ul class="list-inline quicklinks">
                        <li class="list-inline-item"><a href="#">Privacidad</a>
                        </li>
                        <li class="list-inline-item"><a href="#">Términos de uso</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>



    <!-- jQuery -->

    <!-- Bootstrap Core JavaScript -->
    <script src="node_modules/jquery/dist/jquery.js" charset="utf-8"></script>
    <script src="node_modules/tether/dist/js/tether.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Contacto JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- JavaScript -->
<!-- STAR RATING -->
<script src="node_modules/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
<!-- IMPORTANTÍSIMO:
**************************************************



██╗     ███████╗███████╗███╗   ███╗███████╗    ██████╗  █████╗ ███╗   ██╗██╗███████╗██╗
██║     ██╔════╝██╔════╝████╗ ████║██╔════╝    ██╔══██╗██╔══██╗████╗  ██║██║██╔════╝██║
██║     █████╗  █████╗  ██╔████╔██║█████╗      ██║  ██║███████║██╔██╗ ██║██║█████╗  ██║
██║     ██╔══╝  ██╔══╝  ██║╚██╔╝██║██╔══╝      ██║  ██║██╔══██║██║╚██╗██║██║██╔══╝  ██║
███████╗███████╗███████╗██║ ╚═╝ ██║███████╗    ██████╔╝██║  ██║██║ ╚████║██║███████╗███████╗
╚══════╝╚══════╝╚══════╝╚═╝     ╚═╝╚══════╝    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝╚══════╝╚══════╝

/* ACTUALIZACIÓN: */
Sólo falta agregar el número de estrellas que tiene el prestador de servicios. Esto lo harás con el promedio
de las calificaciones totales. Si te confundes con la base de datos que propuse me dices para que haga las consultas.

/* EL TEXTO DE ABAJO ESTÁ OBSOLETO NO LEER */


EN GENERAL:

*La búsqueda ya no será en ese mismo momento, sino que será necesario hacer click en "buscar".
*Para mostrar resultados, se utilizará esta página con el código php pertinente.
*Para mostrar categorías se utilizará esta página con el código php correspondiente (select from...).
*Limitar el título del servicio a dos líneas, que no pase de eso para que tengan el mismo tamaño las tarjetas*
*Limitar el nombre del prestador de servicios a una línea.
*en el costo se pondrá {"x/servicio","x/hora","x/sesión","Desde x","A tratar"}

EN ESTA SECCIÓN, ES NECESARIO IMPRIMIR EL SCRIPT DE ABAJO MÚLTIPLES VECES PARA QUE jquery
REALIZE LA ASIGNACIÓN NECESARIA A LOS MÓDULOS DE RATING DE ESTRELLAS.

LA FORMA ES LA SIGUIENTE, CADA UNO DE LOS MODULOS DE ESTRELLAS GENERADOS, DEBE DE TENER UN ID ÚNICO QUE LLEVE
LA SIGUENTE NOMENCLATURA (SIN LAS LLAVES):

<SELECT ID="EXAMPLE{0-N}">
  <OPTION></OPTION>
  ...
</SELECT>

EL FOR YA HA SIDO CREADO PARA GENERAR DIFERENTES <SCRIPTS> PARA LOS MÓDULOS DE ESTRELLAS, LO ÚNICO QUE HACE FALTA
ES PONER EL CONTEO DE "EMPLEADOS" A MOSTRAR, EN LA VARIABLE $CANTIDAD Y LO DEMÁS SERÁ GENERADO AUTOMÁTICAMENTE.

      *-OJO-*

      PARA ESTABLECER LA CANTIDAD DE ESTRELLAS QUE CADA UNO DE LOS EMPLEADORES TENDRÁ, ES NECESARIO ASIGNARLE EL VALOR
      DESDE PHP (ES POSIBLE EN EL MISMO CÓDIGO DE AQUÍ ABAJO)

      EN LA PARTE DE: initialRating:"x" CON LA RESTRICCIÓN X >= 0

CON ESTO PUEDES OPERAR EL MÓDULO DE ESTRELLAS. OTRA COSA, EN EL FUTURO CUANDO YA HAYAN PUNTUADO A UN VENDEDOR, SE PODRÁ
INHABILITAR EL MÓDULO DE ESTRELLAS MEDIANTE EL JAVASCRIPT EN LA PARTE DE "readonly:'true|false'".
CUIDA LAS MAYÚSCULAS Y MINÚSCULAS EN LOS ATRIBUTOS, O NO LOS CUENTA.
**************************************************
-->
<?php
$cantidad=count($rows);

for ($i=0; $i < $cantidad; $i++){
	$valEstrella=$calif[$i];
	$ms="";
  echo("<script type='text/javascript'>
     $(function() {
        $('#example".$i."').barrating({
          theme: 'fontawesome-stars',
          readonly:'true',
          initialRating:'".$valEstrella."'
        });
     });
  </script>
  ");
}
?>

</body>

</html>