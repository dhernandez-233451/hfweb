<?php
	session_start();
	require_once('../conexion.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <title>Página de inicio</title>

    <!-- Para el Bucstra -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- FUENTES  -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link href="css/agency.css" rel="stylesheet">


</head>

<body id="page-top" class="index">  

    <!-- Barra de nav -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- navegacion colaps -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menú <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top"><?php print @$_SESSION['user']; ?></a>
            </div>

            <!-- navegacion colaps -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#services">Servicios</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#portfolio">Agentes emergentes</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">Acerca de nosotros</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#team">Equipo</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contacto</a>
                    </li>
                </ul>
            </div>
            <!-- / fin de nav -->
        </div>
        <!--  / fin de container-fluid -->
    </nav>
  
    <!-- Servicios emer. -->
    <section id="portfolio" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Mis contratos</h2>
                    <h3 class="section-subheading text-muted">Más texto de relleno bla bla bla</h3>
                </div>
            </div>


            <div class="row">
			<?php
			$ms="";
			if($_SESSION['type']=='t')$ms="SELECT cliente.nombre AS 'cliente', trabajo.id, trabajo.cobro, trabajo.estado, servicio.especialidad, oficio.nombre FROM cliente,trabajo,servicio,oficio,trabajador WHERE cliente.id=trabajo.id_cliente AND trabajo.id_servicio=servicio.id AND servicio.id_oficio=oficio.id AND servicio.id_trabajador=trabajador.id AND trabajador.id=".$_SESSION['id']." ORDER BY trabajo.id DESC;";
			else $ms="SELECT trabajo.id, oficio.nombre, servicio.especialidad, trabajo.cobro, trabajo.estado, trabajador.nombre AS 'trabajador' FROM cliente,trabajo,trabajador,servicio,oficio WHERE cliente.id=trabajo.id_cliente AND trabajo.id_servicio=servicio.id AND servicio.id_trabajador=trabajador.id AND servicio.id_oficio=oficio.id AND cliente.id=".$_SESSION['id']." ORDER BY trabajo.id DESC;";
			$res=mysql_query($ms,$conexion) or die(mysql_error());
			while($r=mysql_fetch_array($res)){
				$usuario=$_SESSION['type']=='t'?(@$r['cliente']):(@$r['trabajador']);
				print '
			<div class="col-md-4 col-sm-6 portfolio-item">
                    <div class="portfolio-caption">
                        <div id="container" style="white-space:nowrap">
                            <div id="image" style="display:inline; float: left;">
                                <img src="img/test100.png"/>
                            </div>
                            <div id="texts" style="padding-left: 9em; text-align: left; white-space:nowrap;">
                              <h4>'.($r['nombre']).'</h4>
                              <p>$'.$r['cobro'].' MXN</p>
                              <p>'.($usuario).'</p>
							  <p>Estado: '.($r['estado']).'</p>
							  <p><a href="detalleh.php?idt='.$r['id'].'">Ver detalle</a></p>
                            </div>
                        </div>
                    </div>
            	</div>
			';
			}
            ?>
        	</div>
    </section>

    

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; DimoluRENACIDO 2017</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="#">Privacidad</a>
                        </li>
                        <li><a href="#">Términos de uso</a>
                        </li>
                        <li><a href="../loginses.php?salir=s">Cerrar Sesión</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

  

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Contacto JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- JavaScript -->
    <script src="js/agency_contr.min.js"></script>

</body>

</html>
