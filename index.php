<?php
session_start();
if(isset($_SESSION["userid"])){
	header("Location:php/");
	exit();
} else{
	session_destroy();
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="shortcut icon" href="assets/img/logo.ico">
	<link rel="icon" href="assets/img/logo.ico">

	<title>Hermes Finder - Iniciar sesión</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
    <link href="php/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/material-kit.css" rel="stylesheet"/>
	<link rel="stylesheet" href="assets/css/test.css">
	<link rel="stylesheet" href="php/node_modules/animsition/dist/css/animsition.min.css">

</head>

<body class="signup-page" <?php if(isset($_GET['u'])) echo 'style="background-color:#f55145;"'; else echo 'style="background-color:#fbbd06"'; ?>>
<div class="animsition">
	<nav class="navbar navbar-transparent navbar-absolute">
    	<div class="container">
        	<!-- Brand and toggle get grouped for better mobile display -->
        	<div class="navbar-header">
        		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
            		<span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
        		</button>
        		<a class="navbar-brand">Hermes Finder</a>
        	</div>

        	<div class="collapse navbar-collapse" id="navigation-example">
        		<ul class="nav navbar-nav navbar-right">
		            <li>
		                <a href="#" target="_blank" class="btn btn-simple btn-white btn-just-icon">
							<i class="fa fa-twitter"></i>
						</a>
		            </li>
		            <li>
		                <a href="#" target="_blank" class="btn btn-simple btn-white btn-just-icon">
							<i class="fa fa-facebook-square"></i>
						</a>
		            </li>
					<li>
		                <a href="#" target="_blank" class="btn btn-simple btn-white btn-just-icon">
							<i class="fa fa-linkedin"></i>
						</a>
		            </li>
        		</ul>
        	</div>
    	</div>
    </nav>

    <div class="wrapper">
		<div class="header header-filter" style="background-image: url('assets/img/city.jpg'); background-size: cover; background-position: top center;">
			<div class="container">

				<div class="row">

					<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
						<div class="card card-signup">
							<form name="myform" class="form" method="post" action="loginses.php">
								<div class="header header-primary text-center">
									<h4><strong>Iniciar sesión</strong></h4>
								</div>
								<p class="text-divider">Ingresa tus datos</p>
								<div class="content">



									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">face</i>
										</span>
										<input type="text" name="usuario" class="form-control" placeholder="Nombre de usuario...">
									</div>

									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">lock_outline</i>
										</span>
										<input type="password" name="contra" placeholder="Contraseña..." class="form-control" />
									</div>
<input type="submit" style="display: none" />
									<p class="text-center text-divider">O crea una <a href="registro.php">nueva cuenta.</a></p>
<!-- POR SI LO LLEGAS A PROGRAMAR
									<div class="checkbox">
										<label>
											<input type="checkbox" name="optionsCheckboxes" checked>
											Recordarme
										</label>
								</div>
							-->
								<div class="footer text-center">
									<a style="color:#fbc02d" href="#" onclick="document.forms['myform'].submit(); return false;" class="btn btn-simple btn-primary btn-lg">Iniciar sesión</a>
								</div>
							</form>
						</div>
						<?php if(isset($_GET["u"]) && $_GET["u"]==1){ echo
						'<div class="text-center alert alert-danger alert-dismissable fade in">
						<strong>Error:</strong> datos incorrectos.
						</div>'; } ?>
					</div>
				</div>
			</div>

			<footer class="footer">
		        <div class="container">
		            <nav class="pull-left">
						<ul>
							<li>
								<a href="#">
									Versión visitante
								</a>
							</li>
							<li>
								<a href="#">
								   Privacidad
								</a>
							</li>
							<li>
								<a href="#">
								   Nosotros
								</a>
							</li>
						</ul>
		            </nav>
		            <div class="copyright pull-right">
		                &copy; 2017 Hermes Finder
		            </div>
		        </div>
		    </footer>

		</div>

    </div>

</div>
</body>

    <script src="php/node_modules/jquery/dist/jquery.min.js" charset="utf-8"></script>
    		<script src="php/node_modules/tether/dist/js/tether.min.js" type="text/javascript"></script>
    <script src="php/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="assets/js/material.min.js"></script>
	<script src="assets/js/material-kit.js" type="text/javascript"></script>
	<script src="php/node_modules/animsition/dist/js/animsition.min.js" type="text/javascript"></script>


	<script type="text/javascript">
		$(document).ready(function() {
  $(".animsition").animsition({
    inClass: 'fade-in',
    outClass: 'fade-out'
  });
});
	</script>

</html>
