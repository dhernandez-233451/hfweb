<?php
session_start();
if(isset($_SESSION["tipo"])) if ($_SESSION["tipo"]=='1') header("Location:index_traba.php"); else echo'';
else header('Location:../');
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <title>Página de inicio</title>

    <!-- Para el Bucstra -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">

    <!-- FUENTES  -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link href="css/main.css" rel="stylesheet">
    <!-- OPEN SANS -->
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link rel="stylesheet" href="node_modules/jquery-bar-rating/dist/themes/fontawesome-stars.css">


</head>
<nav class="navbar fixed-top navbar-toggleable-md navbar-light bg-faded">
  <button class="navbar-toggler navbar-toggler-right custom-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand page-scroll" href="#">Hermes Finder</a>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto"><li class="nav-item"><a class="nav-link" href="#"></a></li></ul>
    </li>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Página principal</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Cerrar sesión</a>
      </li>
    </ul>
  </div>
</nav>
<body>

    <!-- SECCIÓN DE TARJETAS -->
    <section class="tarjetas">
      <!-- MANEJAR DESDE PHP: SI NO HAY AL MENOS 1 ITEM EN MARCHA Y 1 ITEM EN FINALIZADOS
    DESDE PHP PONER EL HEIGHT DEL DIV ADYACENTE AL PRESENTE COMENTARIO EN
    height:100vh; para que cubra toda la pantalla -->
      <div style="" class="container warpx ">
        <nav class="breadcrumb">
          <a class="breadcrumb-item" href="#">Inicio</a>
          <span class="breadcrumb-item active">Mis contrataciones</span>
        </nav>
        <h1><strong>Historial de contrataciones</strong></h1>
        <h3>Contratos en marcha</h3>
        <hr class="hr">
                    <div class="row">
                      <div class="col-sm">
                        <div class="card" style="width: 15rem; border-radius:0px;">
                          <div class="card-block text-left">
                            <h4 class="card-title">Estilista</h4>
                            <h6 style="color:#666;">Josefina Vázquez Mota</h6>
                            <p style="color:#aaa;">[Solicitado]</p>
                            <hr>
                            <a href="">Ver detalles...</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm">
                        <div class="card" style="width: 15rem; border-radius:0px;">
                          <div class="card-block text-left">
                            <h4 class="card-title">Estilista</h4>
                            <h6 style="color:#666;">Josefina Vázquez Mota</h6>
                            <p style="color:#aaa;">[Solicitado]</p>
                            <hr>
                            <a href="">Ver detalles...</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm">
                        <div class="card" style="width: 15rem; border-radius:0px;">
                          <div class="card-block text-left">
                            <h4 class="card-title">Estilista</h4>
                            <h6 style="color:#666;">Josefina Vázquez Mota</h6>
                            <p style="color:#aaa;">[Trabajando]</p>
                            <hr>
                            <a href="">Ver detalles...</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm">
                        <div class="card" style="width: 15rem; border-radius:0px;">
                          <div class="card-block text-left">
                            <h4 class="card-title">Estilista</h4>
                            <h6 style="color:#666;">Josefina Vázquez Mota</h6>
                            <p style="color:#aaa;">[Solicitado]</p>
                            <hr>
                            <a href="">Ver detalles...</a>
                          </div>
                        </div>
                      </div>
                      <!-- FIN DE COLUMNAS -->
                    </div>
                    <h3>Contratos finalizados</h3>
                    <hr class="hr">
                                <div class="row">
                                  <div class="col-sm">
                                    <div class="card" style="width: 15rem; border-radius:0px;">
                                      <div class="card-block text-left">
                                        <h4 class="card-title">Estilista</h4>
                                        <h6 style="color:#666;">Josefina Vázquez Mota</h6>
                                        <p style="color:#aaa;">[Solicitado]</p>
                                        <hr>
                                        <a href="">Ver detalles...</a>
                                      </div>
                                    </div>
                                  </div>

                                  <!-- FIN DE COLUMNAS -->
                                </div>
                  </div>
      </div>
    </section>



    <footer>
        <div class="container">
            <div class="row">
              <div class="col-md-4">
                <ul class="list-inline quicklinks">
                    <li class="list-inline-item">  <span class="copyright">Copyright &copy; Hermes Finder</span>
                    </li>
                </ul>
                </div>
                <div class="col-md-4 text-center">
                    <ul class="list-inline social-buttons">
                        <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 text-center">
                    <ul class="list-inline quicklinks">
                        <li class="list-inline-item"><a href="#">Privacidad</a>
                        </li>
                        <li class="list-inline-item"><a href="#">Términos de uso</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>



    <!-- jQuery -->

    <!-- Bootstrap Core JavaScript -->
    <script src="node_modules/jquery/dist/jquery.js" charset="utf-8"></script>
    <script src="node_modules/tether/dist/js/tether.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Contacto JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- JavaScript -->
<!-- STAR RATING -->
<script src="node_modules/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
<!-- IMPORTANTÍSIMO:
**************************************************



██╗     ███████╗███████╗███╗   ███╗███████╗    ██████╗  █████╗ ███╗   ██╗██╗███████╗██╗
██║     ██╔════╝██╔════╝████╗ ████║██╔════╝    ██╔══██╗██╔══██╗████╗  ██║██║██╔════╝██║
██║     █████╗  █████╗  ██╔████╔██║█████╗      ██║  ██║███████║██╔██╗ ██║██║█████╗  ██║
██║     ██╔══╝  ██╔══╝  ██║╚██╔╝██║██╔══╝      ██║  ██║██╔══██║██║╚██╗██║██║██╔══╝  ██║
███████╗███████╗███████╗██║ ╚═╝ ██║███████╗    ██████╔╝██║  ██║██║ ╚████║██║███████╗███████╗
╚══════╝╚══════╝╚══════╝╚═╝     ╚═╝╚══════╝    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝╚══════╝╚══════╝

/* ACTUALIZACIÓN: */
Sólo falta agregar el número de estrellas que tiene el prestador de servicios. Esto lo harás con el promedio
de las calificaciones totales. Si te confundes con la base de datos que propuse me dices para que haga las consultas.

/* EL TEXTO DE ABAJO ESTÁ OBSOLETO NO LEER */


EN GENERAL:

*La búsqueda ya no será en ese mismo momento, sino que será necesario hacer click en "buscar".
*Para mostrar resultados, se utilizará esta página con el código php pertinente.
*Para mostrar categorías se utilizará esta página con el código php correspondiente (select from...).
*Limitar el título del servicio a dos líneas, que no pase de eso para que tengan el mismo tamaño las tarjetas*
*Limitar el nombre del prestador de servicios a una línea.
*en el costo se pondrá {"x/servicio","x/hora","x/sesión","Desde x","A tratar"}

EN ESTA SECCIÓN, ES NECESARIO IMPRIMIR EL SCRIPT DE ABAJO MÚLTIPLES VECES PARA QUE jquery
REALIZE LA ASIGNACIÓN NECESARIA A LOS MÓDULOS DE RATING DE ESTRELLAS.

LA FORMA ES LA SIGUIENTE, CADA UNO DE LOS MODULOS DE ESTRELLAS GENERADOS, DEBE DE TENER UN ID ÚNICO QUE LLEVE
LA SIGUENTE NOMENCLATURA (SIN LAS LLAVES):

<SELECT ID="EXAMPLE{0-N}">
  <OPTION></OPTION>
  ...
</SELECT>

EL FOR YA HA SIDO CREADO PARA GENERAR DIFERENTES <SCRIPTS> PARA LOS MÓDULOS DE ESTRELLAS, LO ÚNICO QUE HACE FALTA
ES PONER EL CONTEO DE "EMPLEADOS" A MOSTRAR, EN LA VARIABLE $CANTIDAD Y LO DEMÁS SERÁ GENERADO AUTOMÁTICAMENTE.

      *-OJO-*

      PARA ESTABLECER LA CANTIDAD DE ESTRELLAS QUE CADA UNO DE LOS EMPLEADORES TENDRÁ, ES NECESARIO ASIGNARLE EL VALOR
      DESDE PHP (ES POSIBLE EN EL MISMO CÓDIGO DE AQUÍ ABAJO)

      EN LA PARTE DE: initialRating:"x" CON LA RESTRICCIÓN X >= 0

CON ESTO PUEDES OPERAR EL MÓDULO DE ESTRELLAS. OTRA COSA, EN EL FUTURO CUANDO YA HAYAN PUNTUADO A UN VENDEDOR, SE PODRÁ
INHABILITAR EL MÓDULO DE ESTRELLAS MEDIANTE EL JAVASCRIPT EN LA PARTE DE "readonly:'true|false'".
CUIDA LAS MAYÚSCULAS Y MINÚSCULAS EN LOS ATRIBUTOS, O NO LOS CUENTA.
**************************************************
-->
<script type='text/javascript'>
   $(function() {
      $('#example1').barrating({
        theme: 'fontawesome-stars',
        readonly:'true',
        initialRating:'3'
      });
   });
</script>

</body>

</html>
