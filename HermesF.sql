CREATE DATABASE  IF NOT EXISTS `hermesf` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `hermesf`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: hermesf
-- ------------------------------------------------------
-- Server version	5.5.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `servicio`
--

DROP TABLE IF EXISTS `servicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicio` (
  `idServicio` int(11) NOT NULL AUTO_INCREMENT,
  `nombreS` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `precio` varchar(45) DEFAULT NULL,
  `cualitativo` varchar(45) DEFAULT NULL,
  `desc` varchar(300) DEFAULT NULL,
  `masinfo` varchar(300) DEFAULT NULL,
  `trabajador` varchar(30) NOT NULL,
  PRIMARY KEY (`idServicio`,`trabajador`),
  KEY `fk_Servicio_usuario1` (`trabajador`),
  CONSTRAINT `fk_Servicio_usuario1` FOREIGN KEY (`trabajador`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicio`
--

LOCK TABLES `servicio` WRITE;
/*!40000 ALTER TABLE `servicio` DISABLE KEYS */;
INSERT INTO `servicio` VALUES (1,'Entrenador personal','Deporte y fitness','100','hora','Clases multidisciplinarias para cualquier persona que desee ejercitar su cuerpo en un gimnasio, ofrezco diferentes rutinas para distintos objetivos, desde ganar masa muscular, hasta tonificar áreas específicas. Tu objetivo es mi meta.','Nutriólogo certificado por la U.N.A.M. de 23 años con mucha experiencia laboral, conozco de todas todas, este es un texto de prueba x. blablabla blabla','test'),(2,'Guardia o velador','Seguridad y vigilancia','200','base','Vigilar una zona definida, durante diferente tiempo del día. Aumentando su valor en zonas rojas y En la noche.','Vigilante multidsciplinario, 23 años de edad, alto compromiso.','test2'),(3,'Tutor universitario','Eduación','80','clase','Tutorías organizadas en diferentes materias, abarcando un ámplio abanico de las diferentes carras más solicitadas, desde Ingeniería en Sistemas Computacionales hasta Licenciatura en Administración. Así como materias del tronco común.','Mi carrera principal es la nutriología, sin embargo una vida de estudio y compromiso así como un amor inmenso a la filosofía me ha capacitado para poder ofrecer diversas tutorías a alumnos de universidad.','test');
/*!40000 ALTER TABLE `servicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato` (
  `idcontrato` int(11) NOT NULL AUTO_INCREMENT,
  `Servicio_idServicio` int(11) NOT NULL,
  `idcontratado` varchar(30) NOT NULL,
  `idcontratador` varchar(30) NOT NULL,
  `cal_contratado` varchar(1) DEFAULT NULL,
  `cal_contratador` varchar(1) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcontrato`,`Servicio_idServicio`,`idcontratado`,`idcontratador`),
  KEY `fk_contrato_Servicio1` (`Servicio_idServicio`,`idcontratado`),
  KEY `fk_contrato_usuario1` (`idcontratador`),
  CONSTRAINT `fk_contrato_Servicio1` FOREIGN KEY (`Servicio_idServicio`, `idcontratado`) REFERENCES `servicio` (`idServicio`, `trabajador`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contrato_usuario1` FOREIGN KEY (`idcontratador`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visitas`
--

DROP TABLE IF EXISTS `visitas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitas` (
  `idvisitas` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `usuario_idusuario` varchar(30) NOT NULL,
  PRIMARY KEY (`idvisitas`,`usuario_idusuario`),
  KEY `fk_visitas_usuario1` (`usuario_idusuario`),
  CONSTRAINT `fk_visitas_usuario1` FOREIGN KEY (`usuario_idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitas`
--

LOCK TABLES `visitas` WRITE;
/*!40000 ALTER TABLE `visitas` DISABLE KEYS */;
/*!40000 ALTER TABLE `visitas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datos`
--

DROP TABLE IF EXISTS `datos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datos` (
  `iddatos` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `direccion` varchar(300) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `logros` varchar(300) DEFAULT NULL,
  `usuario_idusuario` varchar(30) NOT NULL,
  PRIMARY KEY (`iddatos`,`usuario_idusuario`),
  KEY `fk_datos_usuario` (`usuario_idusuario`),
  CONSTRAINT `fk_datos_usuario` FOREIGN KEY (`usuario_idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datos`
--

LOCK TABLES `datos` WRITE;
/*!40000 ALTER TABLE `datos` DISABLE KEYS */;
INSERT INTO `datos` VALUES (2,'Fransisco Reyes','Lindavista #138','4831182179','Trabajo en equipo,Trabajo en alta presión,Trabajo colaborativo, Egresado de la UNAM','test'),(3,'Juan López','Barrio del Carmen #23','44242424','Alta energía,Capacidades de manejo de armas,Adiestramiento en combate cuerpo a cuerpo','test2'),(4,'Maxwell Winters','Carrington St.','123123123','Gerente','admin');
/*!40000 ALTER TABLE `datos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` varchar(30) NOT NULL,
  `contra` varchar(30) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `sestado` varchar(45) DEFAULT NULL,
  `imagen` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('admin','admin',2,'nolinea','3.jpg'),('test','test',1,'nolinea','1.jpg'),('test2','test2',1,'nolinea','2.jpg');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-07 14:59:41
